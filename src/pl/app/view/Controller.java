package pl.app.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

public class Controller implements Initializable {

	private List<Integer> listData;

	@FXML
	private TextArea listOfData;

	@FXML
	private TextArea noDuplicates;

	@FXML
	private TextArea primeNumbers;

	@FXML
	private Button downloadButton;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		downloadButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				listData = new ArrayList<>();
				try {
					getParsedListData();
				} catch (ParseException | IOException e) {
					e.printStackTrace();
				}

				listOfData.setText(listData.toString());
				noDuplicates.setText(clearDuplicates(listData).toString());
				primeNumbers.setText(selectPrimes(listData).toString());

			}
		});

	}

	private List<Integer> getParsedListData() throws ParseException, IOException {
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject;

		try {
			jsonObject = (JSONObject) jsonParser.parse(getUrl());
			JSONArray jsonArray = (JSONArray) jsonObject.get("data");
			String stringData = jsonArray.toString().replace("[", "").replace("]", "");
			String[] arrayStringData = stringData.split(",");
			for (int i = 0; i < arrayStringData.length; i++) {
				listData.add(Integer.valueOf(arrayStringData[i]));
			}
			Collections.sort(listData);
		} catch (org.json.simple.parser.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return listData;

	}

	private String getUrl() throws IOException {
		URL url = null;
		url = new URL("http://dt-gwitczak-recruitment.westeurope.cloudapp.azure.com:8080/rest/task");
		Authenticator.setDefault(new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("candidate", "abc123".toCharArray());
			}
		});
		URLConnection yc = url.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
		String inputLine = in.readLine();
		in.close();
		return inputLine;
	}

	private List<Integer> selectPrimes(List<Integer> listData) {
		List<Integer> tempList = new ArrayList<>();
		for (int i = 0; i < listData.size(); i++) {
			boolean isPrimeNumber = true;

			for (int j = 2; j < listData.get(i); j++) {
				if (listData.get(i) % j == 0) {
					isPrimeNumber = false;
					break;
				}
			}
			if (isPrimeNumber) {
				tempList.add(listData.get(i));
			}
		}
		listData.clear();
		listData.addAll(tempList);
		return listData;
	}

	private List<Integer> clearDuplicates(List<Integer> listData) {
		Set<Integer> setData = new HashSet<>();
		setData.addAll(listData);
		listData.clear();
		listData.addAll(setData);
		Collections.sort(listData);
		return listData;
	}

}
